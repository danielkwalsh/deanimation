// https://www.opengl.org/sdk/docs/tutorials/TyphoonLabs/Chapter_1.pdf
// https://www2.cs.duke.edu/courses/compsci344/spring15/classwork/15_shading/glsl_cornell.pdf
// http://www.lighthouse3d.com/tutorials/glsl-tutorial
// https://stackoverflow.com/questions/2795044/easy-framework-for-opengl-shaders-in-c-c
// https://github.com/BradLarson/GPUImage

// Fragment shader for blending two images with max
varying vec2 textureACoordinate;
varying vec2 textureBCoordinate;

uniform sampler2D textureA;
uniform sampler2D textureB;

void main()
{
    vec4 textureAColor = texture2D(textureA, textureACoordinate);
    vec4 textureBColor = texture2D(textureB, textureBCoordinate);

    // http://docs.gl/sl4/max
    gl_FragColor = max(textureAColor, textureBColor);
}
