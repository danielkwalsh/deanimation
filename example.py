'''Sample script for processing a video.

Creates a deanimation image as a .png file
and an accumulation visualization video as an .mp4

Creates directories of .bmp images as intermediate output. As they require
ample storage space, they may be deleted upon completion unless desired for
other uses.
'''

import deanimation
import utilities


framerate = '30'
id = '2017.001.003'
input_video = '%s-sample.mp4' % id
extraction_pattern = id + '-frame-%04d.bmp'
extraction_directory = id + '-frames-extracted'
processing_pattern = id + '-processed-%04d.bmp'
processing_directory = id + '-frames-processed'
output_image = '%s-final-blend.png' % id
output_video = id + '-accumulation.mp4'
mode = deanimation.LIGHTEN
n = 10  # include every nth frame in the blend


utilities.extract(input_video, extraction_pattern, extraction_directory)

deanimation.accumulation_single(extraction_directory, output_image, mode, n)

deanimation.accumulation_sequence(extraction_directory, processing_pattern, processing_directory, mode, n)

deanimation.encode(processing_directory, processing_pattern, framerate, output_video)
