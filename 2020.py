
import deanimation
from production import Sequence

# So many birds swirling and some landing above green fields with brown trenches
# s = Sequence(2020, "0042", "DSCF", 24, deanimation.DARKEN, deanimation.TAIL, n=1, t=80)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[0, 0], [146, 255]],
#                     "g":[[0, 0], [151, 255]],
#                     "b":[[0, 0], [169, 255]]},
#           downsize=True)
# s.encode_concise(source="extracted")
# s.deanimation_still(source="extracted")
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated")

# Mostly straight flying starlings from underneath
# s = Sequence(2020, "0097", "DSCF", 24, deanimation.DARKEN, deanimation.TAIL, n=5, t=50)
# s.encode_raw_preview()
# s.extract(curves = {"all":[[0, 0], [135, 255]]})
# s.abridge([[260, 1160]])
# s.encode_concise(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated")
# s.deanimation_still(source="extracted")
# s.deanimate(source="extracted") # Deanimate again w/ no abridgement for crossfade video
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded")

# Uncountable dots of birds over a purple sky
# s = Sequence(2020, "0155", "DSCF", 24, deanimation.DARKEN, deanimation.TAIL, n=4, t=160)
# s.encode_raw_preview()
# s.extract(curves = {"all":[[71, 0], [150, 117], [213, 255]]}, downsize=True)
# s.abridge([[240, 5700]])
# s.encode_concise(source="abridged")
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded")

# Lots of birds wide shot at sunset
# s = Sequence(2020, "0160", "DSCF", 24, deanimation.DARKEN, deanimation.TAIL, n=1, t=320)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[0, 0], [120, 144], [255, 255]],
#                     "g":[[0, 0], [80, 141], [154, 255]],
#                     "b":[[0, 0], [54, 141], [117, 255]]},
#                     downsize=True)
# s.encode_concise(source="extracted")
# s.deanimation_still(source="extracted")
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated")

# Lots of small groups at different distances over orange and purple sky
# s = Sequence(2020, "0162", "DSCF", 24, deanimation.DARKEN, deanimation.TAIL, n=1, t=100)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[32, 0], [93, 60], [179, 176], [255, 255]],
#                     "g":[[8, 0], [71, 70], [155, 186], [216, 255]],
#                     "b":[[0, 0], [37, 71], [80, 191], [111, 255]]},
#                     downsize=True)
# s.abridge([[120, 3060]])
# s.encode_concise(source="abridged")
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded")

# Line of sandhill cranes advance and turn together
# s = Sequence(2020, "0141", "DSCF", 24, deanimation.DARKEN, deanimation.ACCUMULATION, n=5)
# s.encode_raw_preview()
# s.extract(curves={"all":[[18, 0], [83, 141], [182, 255]]})
# s.encode_concise(source="extracted")
# s.deanimation_still(source="extracted")
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)

# Cherry blossoms falling loose
# s = Sequence(2020, "0189", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=2, t=40)
# s.encode_raw_preview()
# s.extract(curves={"all":[[0, 0], [159, 98], [214, 171], [255, 255]]})
# s.abridge([[500, 1640]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated", sharpen=True)
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)

# Cherry blossoms among three trees
# s = Sequence(2020, "0193", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=2, t=40)
# s.encode_raw_preview()
# s.extract(curves={"all":[[0, 0], [145, 111], [255, 255]]})
# s.abridge([[580, 2800]])
# s.multiplex(2, source="abridged")
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated", sharpen=True)
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)
# Multiplexed
# s.encode_concise(source="multiplexed", sharpen=True)
# s.deanimate(source="multiplexed")
# s.encode_result(source="deanimated", sharpen=True)
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)

# # Cherry blossoms but better exposed
# s = Sequence(2020, "0194", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=2, t=60)
# # s.encode_raw_preview()
# s.extract(curves={"all":[[0, 0], [139, 116], [255, 255]]})
# s.abridge([[2600, 4300]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated", sharpen=True)
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)

# Cherry blossoms with graduate posing for photo
# s = Sequence(2020, "0208", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=2, t=60)
# s.encode_raw_preview()
# s.extract(curves={"all":[[0, 0], [140, 117], [255, 255]]})
# s.abridge([[180, 1920]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated", sharpen=True)
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)

# Moths in front of a diagonal tree trunk
# s = Sequence(2020, "0213", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=3, t=75)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[0, 0], [64, 85], [211, 198], [255, 255]],
#                     "g":[[0, 0], [51, 75], [206, 194], [255, 255]],
#                     "b":[[0, 0], [64, 85], [207, 196], [255, 255]]})
# s.encode_concise(source="extracted", sharpen=True)
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated", sharpen=True)
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)


# Midges in a cloud over the basketball court
# s = Sequence(2020, "0218", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=100)
# s.encode_raw_preview()
# # s.extract(curves={"all": [[78, 0], [130, 57], [188, 187], [230, 255]]}) # Black background
# s.extract(curves = {"r":[[4, 0], [68, 63], [144, 184], [207, 255]],
#                     "g":[[0, 0], [62, 72], [132, 181], [190, 255]],
#                     "b":[[5, 0], [64, 66], [136, 184], [190, 255]]})
# s.abridge([[180, 2040]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated", sharpen=True)
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)

# Moths over ivy in low contrast light
# s = Sequence(2020, "0226", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=4, t=40)
# s.encode_raw_preview()
# s.extract(curves={"all":[[7, 0], [66, 83], [122, 187], [166, 255]]})
# s.abridge([[180, 1900]])
# s.encode_concise(source="abridged", sharpen=True)
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated", sharpen=True)
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded", sharpen=True)

# Moths semi-silhouette on evening sky
# s = Sequence(2020, "0236", "DSCF", 24, deanimation.DARKEN, deanimation.TAIL, n=5, t=50)
# s.encode_raw_preview()
# s.extract(curves={"all":[[0, 0], [33, 122], [91, 255]]})
# s.abridge([[200, 2140]])
# s.encode_concise(source="abridged")
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated")
# s.crossfade(source="deanimated")
# s.encode_result(source="crossfaded")

# Lots of bubbles over shallow sea grass
# s = Sequence(2020, "0261", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=7, t=75)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[24, 0], [82, 58], [168, 191], [220, 255]],
#                     "g":[[28, 0], [83, 65], [171, 202], [217, 255]],
#                     "b":[[17, 0], [83, 66], [175, 188], [237, 255]]})  # TODO: reprocess with every_other
# s.abridge([[700, 6840]])
# s.deanimation_still(source="abridged")
# s.encode_concise(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result(source="deanimated")

# Wide perspective without horizon with wild ups and downs and backs and forths
# s = Sequence(2020, "0264", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=3, t=240)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[7, 0], [69, 68], [162, 199], [207, 255]],
#                     "g":[[4, 0], [72, 64], [161, 203], [202, 255]],
#                     "b":[[8, 0], [76, 61], [171, 209], [222, 255]]},
#           every_other=True)
# s.deanimation_still(source="extracted")
# s.encode_concise(source="extracted")
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated")

# Looking inland at a broad canyon mouth
# s = Sequence(2020, "0289", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=10, t=20)
# s.encode_raw_preview()
# s.extract(curves={"all":[[6, 0], [82, 50], [225, 255]]},
#             every_other=True)
# s.abridge([[0, 2500]])
# s.deanimation_still(source="abridged")
# s.encode_concise(source="abridged")
# s.deanimate(source="abridged")
# s.encode_result()

# Many bugs evenly distributed over the frame at sunset at Live Oak
# s = Sequence(2020, "0295", "DSCF", 24, deanimation.LIGHTEN, deanimation.TAIL, n=1, t=200)
# s.encode_raw_preview()
# s.extract(curves = {"r":[[0, 0], [255, 255]],
#                     "g":[[2, 0], [255, 255]],
#                     "b":[[8, 0], [255, 255]]})
# s.deanimate(source="extracted")
# s.encode_result(source="deanimated")
