
import deanimation
from production import Sequence

# # Ladybugs
# s = Sequence(2016, "9113", "MVI_", 24, deanimation.DUAL, deanimation.ACCUMULATION, n=20)
# s.extract()
# s.abridge([[30, 370]])
# s.deanimation_still(source="abridged")

# Gnats in front of tree trunk bark backdrop (overexposed)
# s = production.Sequence(2016, "9133", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=1)
# s.extract(curves= {"all": [[33, 0], [156, 123], [255, 255]]})
# s.abridge([[50, 450]])
# s.deanimation_still(source="abridged")
# s.deanimate(source="abridged")
# s.crossfade(source="deanimated", still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# Gnats in front of tree trunk bark backdrop
# s = Sequence(2016, "9134", "MVI_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=1, explicit_date="2016-10-29", extension="-stabilized.mp4")
# s.extract(curves= {"all": [[10, 0], [97, 80], [195, 194], [255, 255]]})
# s.abridge([[0, 260]])
# s.deanimation_still(source="abridged")
# s.deanimate()
# s.crossfade(source="deanimated", still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)

# Leaves
# 2016-10-29 13.14.34.mov
# s = Sequence(2016, "1314", "IMG_", 24, deanimation.LIGHTEN, deanimation.ACCUMULATION, n=2, extension=".mov")
# s.extract()
# s.abridge([[580, 780]])
# s.encode_concise()
# s.deanimation_still(source="abridged")
# s.deanimate()
# s.crossfade(source="deanimated", still_start=True)
# s.encode_result(source="crossfaded", poster_frame=-30, sharpen=True)
