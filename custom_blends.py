"""Custom blending via numpy arrays"""

import os

from PIL import Image, ImageChops, ImageFilter
import matplotlib
import numpy


def image_frombytes(data):
    """Returns a PIL friendly image from a boolean array
    From: https://stackoverflow.com/questions/50134468/convert-boolean-numpy-array-to-pillow-image
    """
    size = data.shape[::-1]
    databytes = numpy.packbits(data, axis=1)
    return Image.frombytes(mode='1', size=size, data=databytes)


def dual(a, b):
    """Takes two images, `a` and `b` and makes a lighten and a darken blend and
    then overlays the two"""
    lighter = ImageChops.lighter(a, b)
    darker = ImageChops.darker(a, b)

    return overlay(lighter, darker)


def combine(a, b, mask):
    """Takes two numpy arrays, `a` and `b` representing images e.g. [x y rgb]
    and combines the masked a with the inverse masked b.

    The result is that each pixel is taken either from `a` or `b` according
    to the mask.
    """

    # Get the pixel dimension first in preparation for mask multiplication
    a = a.transpose(2, 0, 1)
    b = b.transpose(2, 0, 1)

    # Combine a and b taking from either one or the other according to the mask
    # (If an int is multipled by a bool, it's equivalent to multiplying it by
    # either a 0 or a 1)
    result = (a * mask) + (b * numpy.logical_not(mask))

    # Put the pixel dimension last so it's ordered as an image
    result = result.transpose(1, 2, 0)

    return result


def overlay_sequence(sequence_a_directory, sequence_b_directory, output_directory):
    # Get a list of all the image files in the directories, exclude hidden files
    sequence_a_filenames = [f for f in os.listdir(sequence_a_directory) if not f.startswith(".")]
    sequence_b_filenames = [f for f in os.listdir(sequence_b_directory) if not f.startswith(".")]
    sequence_a_filenames.sort()
    sequence_b_filenames.sort()

    if len(sequence_a_filenames) != len(sequence_b_filenames):
        print("Something's wrong. Sequences a and and b don't match in length")
        return

    for i in range(len(sequence_a_filenames)):
        msg = "Overlaying frame {} of {}. {}".format(i, len(sequence_a_filenames), sequence_a_filenames[i])
        print(msg)
        image_a = Image.open(os.path.join(sequence_a_directory, sequence_a_filenames[i]))
        image_b = Image.open(os.path.join(sequence_b_directory, sequence_b_filenames[i]))
        overlayed_image = overlay(image_a, image_b)
        output_path = os.path.join(output_directory, sequence_a_filenames[i])
        overlayed_image.save(output_path)


def overlay(a_image, b_image):
    """From: https://stackoverflow.com/questions/52141987/overlay-blending-mode-in-python-efficiently-as-possible-numpy-opencv
    """
    a = numpy.array(a_image)
    b = numpy.array(b_image)

    a = a.astype(float)/255
    b = b.astype(float)/255 # make float on range 0-1

    mask = a >= 0.5 # generate boolean mask of everywhere a > 0.5
    ab = numpy.zeros_like(a) # generate an output container for the blended image

    # now do the blending
    ab[~mask] = (2*a*b)[~mask] # 2ab everywhere a<0.5
    ab[mask] = (1-2*(1-a)*(1-b))[mask] # else this

    # Scale to range 0..255 and save
    c = (ab*255).astype(numpy.uint8)

    return Image.fromarray(c)


def compare_channel(a, b, show_mask=False):
    """Compares two numpy arrays representing images, `a` and `b`, and returns
    a new array whose pixels are either from `a` or `b` whichever one's averaged
    rgb value is fruther from `value`.
    """
    # a_avg = numpy.average(a, axis=2)
    # b_avg = numpy.average(b, axis=2)
    # mask = a_avg > b_avg

    # a_amount = a[:,:,2]
    # b_amount = b[:,:,2]
    # mask = a_amount < b_amount

    a_amount = matplotlib.colors.rgb_to_hsv(a)[:,:,2]
    b_amount = matplotlib.colors.rgb_to_hsv(b)[:,:,2]
    mask = a_amount < b_amount

    if show_mask:
        mask_image = image_frombytes(mask)
        mask_image.show()

    return combine(a, b, mask)


def channel(a, b):
    """Blends two images a and b and returns the image whose pixels are taken
    from a or b, whichever pixel's rgb average is further from `value`.
    """
    a_array = numpy.array(a)
    b_array = numpy.array(b)

    c_array = compare_channel(a_array, b_array)

    return Image.fromarray(c_array)


def compare_further_from_average(a, b, value, show_mask=False):
    """Compares two numpy arrays representing images, `a` and `b`, and returns
    a new array whose pixels are either from `a` or `b` whichever one's averaged
    rgb value is fruther from `value`.
    """
    a_avg = numpy.average(a, axis=2)
    b_avg = numpy.average(b, axis=2)
    mask = (abs(value - a_avg)) > (abs(value - b_avg))

    if show_mask:
        mask_image = image_frombytes(mask)
        mask_image.show()

    return combine(a, b, mask)


def further_from_average(a, b, value):
    """Blends two images a and b and returns the image whose pixels are taken
    from a or b, whichever pixel's rgb average is further from `value`.
    """
    a_array = numpy.array(a)
    b_array = numpy.array(b)

    c_array = compare_further_from_average(a_array, b_array, value)

    return Image.fromarray(c_array)


def compare_further_from_color(a, b, color, show_mask=False):
    """Compares two numpy arrays representing images, `a` and `b`, and returns
    a new array whose pixels are either from `a` or `b` whichever one's
    rgb color is fruther from `color` using euclidean distance.

    `color` should be a list representing an rgb color, e.g. [123, 157, 23]
    """

    # Create a reference array of the same size and shape but with all pixels having `color`
    reference = a.copy()
    reference[:,:] = color

    # Compute the euclidean distance for each pixel from each reference pixel
    a_distance = numpy.sqrt(numpy.sum(numpy.square(reference-a), axis=2))
    b_distance = numpy.sqrt(numpy.sum(numpy.square(reference-b), axis=2))

    # Generate the boolean valued image based on whether the pixel from a or b is further
    mask = a_distance > b_distance

    if show_mask:
        mask_image = image_frombytes(mask)
        mask_image.show()

    return combine(a, b, mask)


def further_from_color(a, b, color):
    """Blends two images a and b and returns the image whose pixels are taken
    from a or b, whichever pixel's rgb color is further from the reference `color`
    via euclidean distance.
    """
    a_array = numpy.array(a)
    b_array = numpy.array(b)

    c_array = compare_further_from_color(a_array, b_array, color)

    return Image.fromarray(c_array)


def compare_further_from_image(a, b, image, show_mask=False):
    """Compares two numpy arrays representing images, `a` and `b`, and returns
    a new array whose pixels are either from `a` or `b` whichever one's
    rgb value is fruther from the corresponding pixel in `image` using euclidean
    distance.

    `image` is a numpy array representing a background image to compare against
    """

    # Compute the euclidean distance for each pixel from each reference pixel
    a_distance = numpy.sqrt(numpy.sum(numpy.square(image-a), axis=2))
    b_distance = numpy.sqrt(numpy.sum(numpy.square(image-b), axis=2))

    # Generate the boolean valued image based on whether the pixel from a or b is further
    mask = a_distance > b_distance

    if show_mask:
        mask_image = image_frombytes(mask)
        mask_image.show()

    return combine(a, b, mask)


def blur_compare_further_from_image(a, b, a_blurred, b_blurred, image, show_mask=False):
    # Compute the euclidean distance for each pixel from each reference pixel
    a_blurred_distance = numpy.sqrt(numpy.sum(numpy.square(image - a_blurred), axis=2))
    b_blurred_distance = numpy.sqrt(numpy.sum(numpy.square(image - b_blurred), axis=2))

    # Generate the boolean valued image based on whether the pixel from a or b is further
    mask = a_blurred_distance > b_blurred_distance

    if show_mask:
        mask_image = image_frombytes(mask)
        mask_image.show()

    return combine(a, b, mask)


def further_from_image(a, b, image_path, blur_before_blend=10):
    """Blends two images a and b and returns the image whose pixels are taken
    from a or b, whichever pixel's color is further from the corresponding pixel
    in the reference image at `image_path`.
    """
    # print("further from image with blend: {}".format(blur_before_blend))
    image = Image.open(image_path)

    # Blur a bit before comparing?
    image = image.filter(ImageFilter.GaussianBlur(blur_before_blend))

    image_array = numpy.array(image)

    a_array = numpy.array(a)
    b_array = numpy.array(b)

    a_blurred_array = numpy.array(a.filter(ImageFilter.GaussianBlur(blur_before_blend)))
    b_blurred_array = numpy.array(b.filter(ImageFilter.GaussianBlur(blur_before_blend)))

    # c_array = compare_further_from_image(a_array, b_array, image_array)
    c_array = blur_compare_further_from_image(a_array, b_array, a_blurred_array, b_blurred_array, image_array)

    return Image.fromarray(c_array)


if __name__ == "__main__":
    a = Image.open('comparison-test/a.jpg')
    b = Image.open('comparison-test/b.jpg')

    c = dual(a, b)
    # c = further_from_average(a, b, 109)
    # c = further_from_color(a, b, [99, 106, 115])
    # c = further_from_image(a, b, "comparison-test/background.jpg")

    # Overlay test
    # a = Image.open('test/2018/normal-lighter.png')
    # b = Image.open('test/2018/normal-darker.png')
    # c = overlay(a, b)

    # Render
    c.show()
    c.save('comparison-test/c.jpg')
